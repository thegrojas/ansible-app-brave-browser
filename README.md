<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width="30%" src="./ansible-wordmark.png" alt="Ansible Wordmark"></a>
    <br>
    <img width="70%"src="./path-to-role-logo.png" alt="Role logo">
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-STATUS-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/ROLEID)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-VERSION-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/PROJECTID)]() 
  [![License](https://img.shields.io/badge/license-LICENSE-blue.svg)](/LICENSE)

</div>

---

# Role Name

A brief description of the role goes here.

Also a summary of the tasks performed using a bullet list, like this:

- Task 1
- Task 2
- Task 3

## 🦺 Requirements

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

## 🗃️ Role Variables

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

**Example:**

Available variables are listed below, along with default values (see `defaults/main.yml`):

```yaml
    # Choose the version to be installed
    package_version: '0.1'
    # Default path
    package_path: '/opt/package'
```

## 📦 Dependencies

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

## 🤹 Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }
```

## ⚖️ License

This code is released under the [LICENSE](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

An optional section for the role authors to include contact information, or a website (HTML is not allowed).

## References

- https://github.com/jaredhocutt/ansible-brave-browser
